/**
 * Функция заполнения таблицы магазина данными.
 * В первую очередь находит и очищает таблицу.
 * Потом проверяет есть ли в памяти данные по магазину, если нет то выводит сообщение об этом и заканчивает свою работу.
 * Если данные найдены то они сохраняются в массив и для каждого элемента вызывается функция создающая строку таблицы.
 */
export function store() {
    const table = document.querySelector("tbody");
    table.innerHTML = "";

    if (!localStorage.storeProducts || localStorage.storeProducts === "[]") {
        document.getElementById("box-show").insertAdjacentHTML("beforeend", `<div class="box-none">Поки немає даних ❌</div>`)
        return;
    }
    const products = JSON.parse(localStorage.storeProducts);
  

    products.forEach((product, index) => {
        createTableElement(index + 1, product, "line", editEvent, deleteEvent, table);
    });
}

/**
 * Функция получает данные, создает новый рядок для таблицы, в зависимости от полученых данных создает массив необходимых элементов, каждый элемент массива создается путем вызова функции создающей ячейку таблицы с передачей в нее необходимых данных, после чего ячейки добавляются в строку, а строка добавляется в таблицу.
 * @param {number} number Порядковый номер элемента
 * @param {object} product Объект в котором содержаться данные продукта
 * @param {string} className Имя класса для строки, при необходимости
 * @param {callback} editEvent Функция для обработки события редактирования элемента
 * @param {callback} deleteEvent Функция для обработки события удаления элемента
 * @param {object} table Таблица в которую будет добавляться строка.
 */
function createTableElement(number, product, className, editEvent, deleteEvent, table) {
    const { name, quantity, price, status, description, date, category, url } = product;
    const tr = document.createElement("tr");

    const editButton = document.createElement("button");
    editButton.innerText = "Редагувати";
    editButton.style.cursor = "pointer";
    editButton.addEventListener("click", () => {
        editEvent(product);
    });

    const delButton = document.createElement("button");
    delButton.innerText = "Видалити";
    delButton.style.cursor = "pointer";
    delButton.addEventListener("click", () => {
        deleteEvent(product);
    });

    if (window.location.pathname.includes("store")) {
        const elements = [
            newTd(number),
            newTd(name),
            newTd(quantity),
            newTd(price),
            newTd(editButton),
            newTd(status ? "✅" : "❌"),
            newTd(date),
            newTd(delButton)
        ]
        tr.append(...elements);
    }
    else if (window.location.pathname.includes("restoran")) {
        const elements = [
            newTd(number),
            newTd(name),
            newTd(quantity),
            newTd(price),
            newTd(editButton),
            newTd(status ? "✅" : "❌"),
            newTd(date),
            newTd(delButton)
        ]
        tr.append(...elements);
    }
    else if (window.location.pathname.includes("video")) {
        const elements = [
            newTd(number),
            newTd(name),
            newTd(date),
            newTd(url),
            newTd(editButton),
            newTd(delButton)
        ]
        tr.append(...elements);
    }
    table.append(tr);
}

/**
 * Функция получает на вход данные, создает ячейку таблицы, в зависимости от типа данных тем или иным способом вставляет их в ячейку.
 * @param {*} props Данные которые нужно поместить в ячейку таблицы.
 * @returns Ячейку таблицы с содержимым.
 */
function newTd(props) {
    if (!props) return;
    const td = document.createElement("td");
    if (props.localName === "button") {
        td.append(props);
    }
    else {
        td.innerText = props;
    }
    return td;
}

/**
 * Функция принимает на вход объект, получает данные из памяти. находит по ID этот объект, удаляет его, загружает данные обратно в память и запускает функцию по заполнению таблицы.
 * @param {object} product Продукт который нужно удалить
 */
function deleteEvent(product) {
    const { id } = product;
    const allProducts = JSON.parse(localStorage.storeProducts);

    allProducts.splice(allProducts.findIndex(el => id === el.id), 1);
    localStorage.storeProducts = JSON.stringify(allProducts);
    store();
}

/**
 * Функция принимает на вход объект, получает данные из памяти. находит по ID этот объект, удаляет его из памяти сохранив при этом в отдельный массив. Далее идет открытие модального окна и вызов функции по отображению данных этого объекта с передачей его в эту функцию.
 * @param {object} product Продукт который нужно отредактировать.
 */
function editEvent(product) {
    //name, quantity, price, status, description, date, category, url 
    let {id} = product;
    const allProducts = JSON.parse(localStorage.storeProducts);
    const [oldObj] = allProducts.splice(allProducts.findIndex(el => id === el.id), 1);
    const modal = document.querySelector(".container-modal");
    const close = document.querySelector("#modal-close");

    modal.classList.remove("hide");
    close.addEventListener("click", () => {
        modal.classList.add("hide");
    });
    showPropertyProduct(oldObj);
}

/**
 * Функция получает на фход объект данные которого нужно отобразить для редактирования.
 * Находит форму в которую нужно вывести данные, потом разбирает объект на свойства.
 * Каждая пара ключ:значение передается в функцию которая создает поля для их отображения, эти поля записываются в массив который потом включается в форму.
 * @param {object} p Продукт данные которого нужно вывести в модальном окне
 */
function showPropertyProduct (p) {
    const edit = document.getElementById("edit");
    const props = Object.entries(p);
    console.log(props);
    const formData =  props.map(([a1, a2])=>{
      return  createPropertyElement(a1, a2)
    });

    edit.append(...formData);
}

/**
 * Функция принимает на вход два параметра, которые являются ключом и значением одного свойства продукта, и коллбэк функцию...
 * Создает блок включающий input и label в которые записываются значения.
 * @param {string} name Имя ключа присваиваемое Label-у
 * @param {string} value Значение которое записывается в Input и может быть отредактировано
 * @param {callback} fnEvent 
 * @returns DIV содержащий Input с данными для редактирования и Label к нему
 */
function createPropertyElement (name, value, fnEvent) {
    const id = Math.floor(Math.random()*1000000);
    const div = document.createElement("div");
    const label = document.createElement("label");
    const input = document.createElement("input");
    label.setAttribute("for", id);
    input.id = id;
    input.value = value;
    label.innerText = name;
    div.append(label, input);
    return div;
}
