const buttonSend = document.querySelector("#submit");

/**
 * В зависимости от того какой выбран Select функция добавляет в тело модалки результат выполнения соответствующей функции по созданию формы
 * @param {string} productType Получает данные из селекта, по умолчанию задан "Магазин"
 */
export function showForm(productType = "магазин") {
    sendBtnDisabled()
    const productBody = document.querySelector(".product-body");
    productBody.innerHTML = "";
    if (productType === "магазин") {
        productBody.append(...createStoreForm());
       
    } else if (productType === "ресторани") {
        productBody.append(...createRestoranForm());
      

    } else if (productType === "відео хостинг") {
        productBody.append(...createVideoForm())
      
    }
    buttonSend.addEventListener("click", () => {
        sendData(productType);
        document.querySelector(".container-modal").classList.add("hide");
    });

}

/**
 * Функция создает элементы формы по добавлению продуктов в Магазин, при помощи вызова колбэка с разными параметрами
 * @returns Возвращает массив элементов формы
 */
function createStoreForm() {
    return [
        createElementForm(
            "Введіть назву продукта",
            "text",
            "product-name",
            "текст має бути Українською"),
        createElementForm(
            "Введіть ціну",
            "number",
            "product-price",
            "тут мають бути лише числа"),
        createElementForm(
            "Опис товару",
            undefined,
            "product-description",
            "текст має бути Українською")
    ]
}
/**
 * Функция создает элементы формы по добавлению продуктов в Ресторан, при помощи вызова колбэка с разными параметрами
 * @returns Возвращает массив элементов формы
 */
function createRestoranForm() {
    return [
        createElementForm(
            "Введіть назву продукта",
            "text",
            "product-name",
            "текст має бути Українською"),
        createElementForm(
            "Введіть ціну",
            "number",
            "product-price",
            "тут мають бути лише числа"),
        createElementForm(
            "Опис товару",
            undefined,
            "product-description",
            "текст має бути Українською")
    ]
}
/**
 * Функция создает элементы формы по добавлению фильмов в Прокат, при помощи вызова колбэка с разными параметрами
 * @returns Возвращает массив элементов формы
 */
function createVideoForm() {
    return [
        createElementForm(
            "Назва відео",
            "text",
            "video-name",
            "текст має бути Українською"),
        createElementForm(
            "Посилання на відео",
            "url",
            "video-url",
            "Вкажіть в форматі https://name.org")
    ]
}

/**
 * Функция принимает параметры на основании которых создает блок включающий Label, Input и DIV для вывода ошибки. Также тут навешивается на Input функция для валидации введенных данных
 * @param {string} placeholder Placeholder для Input, который становится текстом для Label, по умолчанию пустая строка
 * @param {string} type Тип Input, по умолчанию "text"
 * @param {string} classInput Если нужно передаем класс для Input
 * @param {string} errorText Передаем текст который будет отображаться в случае неправильного ввода в Input
 * @param {callback} eventInput Передаем callback функцию, непонятно для чего...
 * @returns DIV элемент, включающий в себя label, input, и div для отображения ошибки
 */
function createElementForm(placeholder = "", type = "text", classInput = "", errorText = "Помилка", eventInput = () => { }) {
    const parent = document.createElement("div"),
        label = document.createElement("label"),
        input = document.createElement("input"),
        error = document.createElement("div");

    parent.classList.add("input");
    label.innerText = placeholder;
    input.type = type;
    input.validate = false;
    input.classList.add(classInput);
    error.classList.add("error", "hide");
    error.innerText = errorText;
    input.addEventListener("change", eventInput);
    input.addEventListener("input", (e) => {
        validate(e.target.value, e.target.classList.value, error, input);
    });
    input.addEventListener("focus", () => {
        label.classList.add("none-placeholder");
    });
    input.addEventListener("blur", () => {
        if (!input.value) {
            label.classList.remove("none-placeholder");
        }
    });

    parent.append(label, input, error);

    return parent;
}

/**
 * Небольшая функция которая генерирует ID из 8 символов, случайным образом из цифр и букв в разном регистре.
 * @returns Строку длиной 8 символов.
 */
function createId() {
    let symbols = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890";
    let idLength = 8;

    let string = "";

    for (let i = 0; i < idLength; i++) {
        let n = Math.floor(Math.random() * symbols.length);
        string += symbols[n];
    }
    return string;
}

/**
 * @class StoreProduct
 * @constructor принимает параметры name, price, description, date
 * @param {string} name Имя продукта
 * @param {number} price Стоимость продукта
 * @param {string} description Описание продукта
 * @param {string} date Дата добавления продукта, создается автоматически при добавлении продукта в базу и форматируется в строку в нужном формате
 * @description При создании экземпляра класса мы указываем наименование, стоимость и описание продукта, дата ставится текущая на момент добавления продукта в базу, ID продукта формируется автоматически с помощью специальной функции, остальные характеристики можно добавлять при редактировании продукта
 */
class StoreProduct {
    constructor(name, price, description, date) {
        this.id = createId();
        this.name = name;
        this.price = price;
        this.status = false;
        this.date = date;
        this.description = description;
        this.category = this.constructor.name;
        this.quantity = "0";
    }
}
class RestProduct {
    constructor(name, price, description, date) {
        this.id = createId();
        this.name = name;
        this.price = price;
        this.status = false;
        this.date = date;
        this.description = description;
        this.category = this.constructor.name;
        this.quantity = "0";
    }
}
class VideoProduct {
    constructor(name, url, date) {
        this.id = createId();
        this.name = name;
        this.url = url;
        this.date = date;
        this.category = this.constructor.name;
    }
}

/**
 * Массив в который поднимаются данные из localStorage, потом добавляется новый продукт и снова заливается в память
 */
let storeProducts = [];
let restProducts = [];
let videoProducts = [];

/**
 * Функция вызывается при правильно заполненых полях формы по нажатию на кнопку отправить.
 * В первую очередь создается и форматируется дата добавления продукта.
 * Потом в зависимости от типа продукта создаются переменные в которые записываются значения из Input-ов, эти переменные служат параметрами при создании нового экземпляра класса.
 * Далее проверяется есть ли уже в памяти продукты данного типа, если да то они записываются в массив, потом туда добавляется новый продукт, и массив снова записывается в память.
 * В конце форма очищается и кнопка становится не активной.
 * @param {string} type При вызове функции передается тип добавляемого продукта
 */
function sendData(type) {
    let date = new Date();
    date = date.toLocaleString().split(",")[0].split(".").reverse().join(".") + date.toLocaleString().split(",")[1];
    if (type === "магазин") {
        const name = document.querySelector(".product-name").value;
        const price = document.querySelector(".product-price").value;
        const desc = document.querySelector(".product-description").value;
        if (localStorage.storeProducts) {
            storeProducts = JSON.parse(localStorage.getItem("storeProducts"));
        }
        storeProducts.push(new StoreProduct(name, price, desc, date));
        localStorage.setItem("storeProducts", JSON.stringify(storeProducts));
        document.querySelector(".product-name").value = ""
        document.querySelector(".product-price").value = ""
        document.querySelector(".product-description").value = ""
        sendBtnDisabled()
    }
    else if (type === "ресторани") {
        const name = document.querySelector(".product-name").value;
        const price = document.querySelector(".product-price").value;
        const desc = document.querySelector(".product-description").value;
        if (localStorage.restProducts) {
            restProducts = JSON.parse(localStorage.getItem("restProducts"));
        }
        restProducts.push(new RestProduct(name, price, desc, date));
        localStorage.setItem("restProducts", JSON.stringify(restProducts));
        sendBtnDisabled()
    }
    else if (type === "відео хостинг") {
        const name = document.querySelector(".video-name").value;
        const url = document.querySelector(".video-url").value;
        if (localStorage.videoProducts) {
            videoProducts = JSON.parse(localStorage.getItem("videoProducts"));
        }
        videoProducts.push(new VideoProduct(name, url, date));
        localStorage.setItem("videoProducts", JSON.stringify(videoProducts));
        sendBtnDisabled()
    }
}

/**
 * Данная функция вызывается при любом изменении в форме добавления товара. Она проверяет соответствие введенных данных необходимым параметрам. Если возникает ошибка то появляется поле в котором она выводится, если все правильно то поле с ошибкой прячется а Input становится validate. В конце вызывается функция разблокировки кнопки отправки.
 * @param {string} value Введенное на момент вызова значение
 * @param {string} classList Классы имеющиеся у Input-a в который сейчас вводятся данные
 * @param {object} error Поле для отображения ошибки. Передается для скрытия \ отображения.
 * @param {object} input Поле ввода. Передается для возможности присвоить статус validate.
 */
const validate = (value, classList, error, input) => {
    if (classList.includes("name")) {
        const patern = /^[а-яіїєґ0-9- ]{3,20}$/i;
        if (patern.test(value)) {
            error.classList.add("hide");
            input.validate = true;
        }
        else {
            input.validate = false;
            error.classList.remove("hide");
        }
    }
    if (classList.includes("price")) {
        const patern = /^[0-9.]{1,6}$/i;
        if (patern.test(value)) {
            error.classList.add("hide");
            input.validate = true;
        }
        else {
            error.classList.remove("hide");
            input.validate = false;
        }
    }
    if (classList.includes("description")) {
        const patern = /^[а-яіїєґ0-9- ]{5,}$/i;
        if (patern.test(value)) {
            error.classList.add("hide");
            input.validate = true;
        }
        else {
            error.classList.remove("hide");
            input.validate = false;
        }
    }
    if (classList.includes("url")) {
        const patern = /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i;
        if (patern.test(value)) {
            error.classList.add("hide");
            input.validate = true;
        }
        else {
            error.classList.remove("hide");
            input.validate = false;
        }
    }
    buttonShow();
}


/**
 * Функция активации кнопки отправки данных из формы.
 * Перебирает все Input-ы проверяя их на статус validate, если каждый элемент вернет true то кнопка разблокируется
 */
function buttonShow() {
    const [...inputs] = document.querySelectorAll("input");
    const rez = inputs.every(a => {
        return a.validate === true;
    });

    if(rez){
        buttonSend.disabled = false;
    }
    else{
        buttonSend.disabled = true;
    }
}

/**
 * Стрелочная функция которая блокирует кнопку отправки.
 * Вызывается в самом начале при построении формы.
 */
const sendBtnDisabled = () => {
    buttonSend.disabled = true;
}



